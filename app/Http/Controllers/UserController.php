<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UserStoreRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
	public function index(): Application|ResponseFactory|Response
	{
		return response(User::all());
	}

	public function store(UserStoreRequest $request): Response|Application|ResponseFactory
	{
		$data = $request->only('email', 'name', 'tel');
		$data['password'] = Hash::make($request->password);

		User::create($data);

		return $this->ok(201);
	}

	public function show(User $user): Response|Application|ResponseFactory
	{
		return response($user);
	}

	public function update(Request $request, User $user)
	{
		$user->update($request->only('email', 'name', 'tel', 'password'));

		return $this->ok();
	}

	public function destroy(User $user): Response|Application|ResponseFactory
	{
		$user->delete();

		return $this->ok();
	}
}
