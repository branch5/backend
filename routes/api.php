<?php

use App\Http\Controllers\UserController as UserControllerAlias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::apiResource('user', UserControllerAlias::class);
